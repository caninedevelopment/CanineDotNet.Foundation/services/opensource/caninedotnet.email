namespace Canine.Net.Client.Resources
{
    using Canine.Net.Infrastructure.Console;
    using Monosoft.Email.Domain.Interfaces;

    internal class EmailResource : BaseResource
    {
        public EmailResource(IEmailSend emailSend) : base("Email")
        {
            this.AddCommand(new Monosoft.Email.Application.Resources.Email.Commands.Send.Command(emailSend), OperationType.Execute, "SendEmail", "Placeholder for a description");//TODO Claims and OperationType
        }
    }
}
