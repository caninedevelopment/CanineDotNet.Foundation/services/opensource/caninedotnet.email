using static Canine.Net.Infrastructure.Console.Program;
using Canine.Net.Infrastructure.Console;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Canine.Net.Infrastructure.Config;
using static Infrastructure.Implementation.DependencyInjection;
namespace Canine.Net.Client
{
    public static class Program
    {
        public static void Main()
        {
            var smtpservername = System.Environment.GetEnvironmentVariable("Email_smtp_servicename");
            var smtpusername = System.Environment.GetEnvironmentVariable("Email_smtp_user");
            var smtppassword = System.Environment.GetEnvironmentVariable("Email_smtp_password");
            var smtpport = System.Environment.GetEnvironmentVariable("Email_smtp_port");

            var rabbitMQUsername = System.Environment.GetEnvironmentVariable("rabbitmq_username");
            var rabbitMQPassword = System.Environment.GetEnvironmentVariable("rabbitmq_password");
            var rabbitMQHost = System.Environment.GetEnvironmentVariable("rabbitmq_host");
            var rabbitMQPort = System.Environment.GetEnvironmentVariable("rabbitmq_port");

            RabbitMQSettings rabbitSet = new RabbitMQSettings(rabbitMQUsername, rabbitMQPassword, rabbitMQHost, rabbitMQPort != null ? int.Parse(rabbitMQPort) : 5672);

            ServiceProvider serviceProvider = new ServiceCollection().AddImplementation(smtpservername, smtpusername, smtppassword, int.Parse(smtpport)).AddResources().BuildServiceProvider();
            StartRabbitMq("EmailService",new ProgramVersion(1), serviceProvider.GetServices<IResource>().ToList(), rabbitSet);
        }
    }
}