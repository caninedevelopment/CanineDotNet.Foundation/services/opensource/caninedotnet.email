using Canine.Net.Infrastructure.Console;
using Canine.Net.Client.Resources;
using Microsoft.Extensions.DependencyInjection;

namespace Canine.Net.Client
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddResources(this IServiceCollection services)
        {
            services.AddTransient<IResource, EmailResource>();
            return services;
        }
    }
}